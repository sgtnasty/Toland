import Foundation

public class Starship {
    
    public enum StarshipClass {
        case Fighter
        case Corvette
        case Frigate
        case Destroyer
        case Cruiser
        case HeavyCruiser
        case Battleship
        case Carrier
        case Dreadnought
    }
    
    public enum States {
        case Normal
        case ShieldsDown
        case StreamingAtmosphere
        case DriveFieldFluctuates
        case DriveFieldDown
    }
    
    public enum StarshipSystems {
        case Sheilds
        case Armor
        case Power
        case Engines
        case Bridge
        case Quarters
        case MissileLauncher
        case TorpedoLauncher
        case ForceBeam
        case LaserBeam
    }
    
    public var name: String = ""
    public var configuration: String = ""
    public var starshipclass = StarshipClass.Corvette
    
    public init (name: String) {
        self.name = name
    }

    public func AddConfig(confType: StarshipSystems, amount: Int) {
        let s = String(describing: confType).prefix(1)
        for _ in 1...amount  {
            self.configuration += String(s)
        }
    }
    
    func GetSysCount(sysType: StarshipSystems) -> Int {
        let key = String(describing: sysType).prefix(1)
        let sys = Array(self.configuration)
        var count = 0
        for s in sys {
            if String(s) == String(key) {
                count += 1
            }
        }
        return count
    }
    
    public func GetSpeed() -> Int {
        /*
        let key = String(describing: StarshipSystems.Engines).characters.prefix(1)
        var speed = 0
        let sys = Array(self.configuration.characters)
        for s in sys {
            if String(s) == String(key) {
                speed += 1
            }
        }
        return speed
         */
        return GetSysCount(sysType: StarshipSystems.Engines)
    }
 }

