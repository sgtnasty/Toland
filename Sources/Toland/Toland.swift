// The Swift Programming Language
// https://docs.swift.org/swift-book
//: Playground - noun: a place where people can play

import Foundation

var VERSION = "Starfire"


func random(_ range:Range<Int>) -> Int
{
    //return range.lowerBound + Int(arc4random_uniform(UInt32(range.upperBound - range.lowerBound)))
    return Int.random(in: range)
}

func simulationA()
{
    print("\(VERSION)")
    let count = 1000
    var total = 0
    var min = Int.max
    var max = 0
    for _ in 1...count {
        let d1 = random(1..<6)
        let d2 = random(1..<6)
        let d3 = random(1..<6)
        let x = d1 + d2 + d3
        //print("Roll 3d6: (\(d1), \(d2), \(d3)) = \(x)")
        total += x
        if (x < min) {min = x}
        if (x > max) {max = x}
    }
    let avg = Double(total) / Double(count)
    print("\(count) rolls, min = \(min), average = \(avg), max = \(max)")

    let s1 = Starship(name: "Athava")
    s1.starshipclass = Starship.StarshipClass.Cruiser

    s1.AddConfig(confType: Starship.StarshipSystems.Sheilds , amount: 5)
    s1.AddConfig(confType: Starship.StarshipSystems.Armor , amount: 3)
    s1.AddConfig(confType: Starship.StarshipSystems.Bridge , amount: 2)
    s1.AddConfig(confType: Starship.StarshipSystems.Power , amount: 5)
    s1.AddConfig(confType: Starship.StarshipSystems.Quarters , amount: 1)
    s1.AddConfig(confType: Starship.StarshipSystems.Armor , amount: 1)
    s1.AddConfig(confType: Starship.StarshipSystems.LaserBeam , amount: 2)
    s1.AddConfig(confType: Starship.StarshipSystems.ForceBeam , amount: 1)
    s1.AddConfig(confType: Starship.StarshipSystems.Bridge , amount: 1)
    s1.AddConfig(confType: Starship.StarshipSystems.TorpedoLauncher , amount: 1)
    s1.AddConfig(confType: Starship.StarshipSystems.MissileLauncher , amount: 2)
    s1.AddConfig(confType: Starship.StarshipSystems.Armor , amount: 1)
    s1.AddConfig(confType: Starship.StarshipSystems.Engines , amount: 4)

    print("Starship \"\(s1.name)\":\(s1.starshipclass) = \(s1.configuration)")
    print("\t speed = \(s1.GetSpeed())")
}

